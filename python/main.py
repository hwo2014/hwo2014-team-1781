import json
import socket
import sys
import math
#import pylab as pl


class NoobBot(object):
    
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.track = []
        self.angles = []
        self.distances = []
        self.sw = []
        self.speed = 0
        self.index = 0
        self.turbo = False
        self.acelerador = 1
                

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")                    
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()
    
    #----------------------------------Zona de codigo modificable------------------------
    
    def on_car_positions(self, data):
        
        self.index = data[0]['piecePosition']['pieceIndex']                
        
        try:    
            if self.angles[self.index+1] == 0:
                self.throttle(0.9)
            else:
                self.throttle(0.65)
                if self.angles[self.index+3] >= 0:
                    print "drch"
                    self.switch(1)
                else:
                    print "izd"
                    self.switch(0)
        except:
            if self.angles[self.index] == 0:
                self.throttle(0.9)
            else:
                self.throttle(0.5)
                if self.angles[self.index] >= 0:
                    print "drch"
                    self.switch(0)
                else:
                    print "izd"
                    self.switch(1)      
       
           
    def switch(self,lane):
        if lane == 0:
            self.msg("switchLane","Left")
        else:
            self.msg("switchLane","Right")        
        
    def turboAvailable(self,data):
        print "Turbo disponible"
        self.turbo = True       
        
    def lapFinished(self,data):
        self.lap = True
        
    def on_game_init(self,data):
        self.track = data
        carretera = self.track['race']['track']['pieces']
        j = 0
        
        for i in carretera:
            if 'length' in i:
                self.angles.append(0)
                self.distances.append(i['length'])
            if 'switch' in i:
                self.sw.append(j)
            if 'angle' in i:
                self.angles.append(i['angle'])
                self.distances.append(i['radius'])
            j = j + 1
        
        print self.angles
        print self.sw       

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit':self.on_game_init,
            'carPositions': self.on_car_positions,
            'turboAvailable':self.turboAvailable,
            'lapFinished': self.lapFinished,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)                                
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))                
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()